package main

import (
	"flag"
	"fmt"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"os"
	"strings"

	"code.google.com/p/go.image/bmp"
	"code.google.com/p/go.image/tiff"
)

func version() {
	os.Stderr.WriteString(`imconv: v0.1.0
`)
}

func usage() {
	os.Stderr.WriteString(`Create converted image from one format to another.
Usage: imconv [OPTION...] INPUTFILE

Supported format:
	bmp gif png jpeg tiff

Options:
	-t         format of the output (require!)
	-f         format of the input (if omitted it, decide by INPUTFILE)
	-o         name of the output file (if omitted it, decide by INPUTFILE)
	--help     show this help message
	--version  print the version

Report bugs to <kusabashira227@gmail.com>
`)
}

type Convertor struct {
	Reader func(in *os.File) (image.Image, error)
	Writer func(out *os.File, img *image.Image) error
}

var convertor = map[string]*Convertor{
	"bmp": {
		Reader: func(in *os.File) (image.Image, error) {
			return bmp.Decode(in)
		},
		Writer: func(out *os.File, img *image.Image) error {
			return bmp.Encode(out, *img)
		},
	},
	"tiff": {
		Reader: func(in *os.File) (image.Image, error) {
			return tiff.Decode(in)
		},
		Writer: func(out *os.File, img *image.Image) error {
			return tiff.Encode(out, *img, nil)
		},
	},
	"gif": {
		Reader: func(in *os.File) (image.Image, error) {
			return gif.Decode(in)
		},
		Writer: func(out *os.File, img *image.Image) error {
			return gif.Encode(out, *img, nil)
		},
	},
	"png": {
		Reader: func(in *os.File) (image.Image, error) {
			return png.Decode(in)
		},
		Writer: func(out *os.File, img *image.Image) error {
			return png.Encode(out, *img)
		},
	},
	"jpg": {
		Reader: func(in *os.File) (image.Image, error) {
			return jpeg.Decode(in)
		},
		Writer: func(out *os.File, img *image.Image) error {
			return jpeg.Encode(out, *img, nil)
		},
	},
	"jpeg": {
		Reader: func(in *os.File) (image.Image, error) {
			return jpeg.Decode(in)
		},
		Writer: func(out *os.File, img *image.Image) error {
			return jpeg.Encode(out, *img, nil)
		},
	},
}

func ConvertImageFile(inputFile, outputFile string, from, to *Convertor) error {
	in, err := os.Open(inputFile)
	if err != nil {
		return err
	}
	defer in.Close()

	img, err := from.Reader(in)
	if err != nil {
		return err
	}

	out, err := os.Create(outputFile)
	if err != nil {
		return err
	}
	defer out.Close()

	return to.Writer(out, &img)
}

func extractName(fname string) string {
	if strings.Contains(fname, ".") {
		return fname[:strings.LastIndex(fname, ".")]
	}
	return fname
}

func extractExtension(fname string) string {
	if strings.Contains(fname, ".") {
		return fname[strings.LastIndex(fname, ".")+1:]
	}
	return ""
}

func _main() (exitCode int, err error) {
	var (
		isHelp        = flag.Bool("help", false, "print the version")
		isVersion     = flag.Bool("version", false, "print the version")
		rawFromFormat = flag.String("f", "", "format of the input")
		rawToFormat   = flag.String("t", "", "format of the output")
		rawOutputName = flag.String("o", "", "neme of the output file")
	)
	flag.Usage = usage
	flag.Parse()

	if *isHelp {
		usage()
		return 0, nil
	}

	if *isVersion {
		version()
		return 0, nil
	}

	if flag.NArg() < 1 {
		return 1, fmt.Errorf("no input file")
	}

	inputFile := flag.Arg(0)

	toFormat := strings.ToLower(*rawToFormat)
	if toFormat == "" {
		return 1, fmt.Errorf("no specified output format")
	}
	if convertor[toFormat] == nil {
		return 1, fmt.Errorf("unsupported output format '%s'", toFormat)
	}

	fromFormat := strings.ToLower(*rawFromFormat)
	if fromFormat == "" {
		fromFormat = strings.ToLower(extractExtension(inputFile))
	}
	if convertor[fromFormat] == nil {
		return 1, fmt.Errorf("input: unsupported format '%s'", fromFormat)
	}

	outputFile := ""
	if *rawOutputName == "" {
		outputFile = extractName(inputFile) + "." + toFormat
	} else {
		outputFile = *rawOutputName + "." + toFormat
	}

	err = ConvertImageFile(inputFile, outputFile,
		convertor[fromFormat], convertor[toFormat])
	if err != nil {
		return 1, err
	}

	return 0, nil
}

func main() {
	exitCode, err := _main()
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
	}
	os.Exit(exitCode)
}
