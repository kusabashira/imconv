imconv
====================
Create converted image from one format to another.

###Supported format
- bmp
- gif
- png
- jpeg
- tiff

Usage
====================
	$ imconv [OPTION...] INPUTFILE

	Options:
		-t         format of the output (require!)
		-f         format of the input (if omitted it, decide by INPUTFILE)
		-o         name of the output file (if omitted it, decide by INPUTFILE)
		--help     show this help message
		--version  print the version

Example
====================
	$ imconv -t bmp mypict.png
	convert to mypict.png mypict.bmp

	$ imconv -t tiff mypict.bmp
	convert to mypict.bmp mypict.tiff

	$ imconv -t bmp -f png mypict
	convert to mypict mypict.bmp

	$ imconv -t bmp -o memory mypict.png
	convert to mypict.png memory.bmp

License
====================
MIT License

Author
====================
Kusabashira <kusabashira227@gmail.com>
